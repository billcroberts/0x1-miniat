########################################################################
#  Build the MiniAT tests
########################################################################

set(SOURCES
	# miniat_tests.c created from vm/test/ via...
	#     python3 cutest-1.5/make-tests.py -o miniat_tests.c `find miniat* -name "*.c" | sort`
	miniat_tests.c

	cutest-1.5/CuTest.c

	miniat_error/test_error.c
	miniat_execution/branch_delay.c
	miniat_execution/bus_read.c
	miniat_execution/bus_write.c
	miniat_execution/flush.c
	miniat_execution/ints_high_low.c
	miniat_execution/pins_interface.c
	miniat_execution/store_load_asm.c
	miniat_execution/timers.c
	miniat_execution/watchdog.c
	miniat_pc/test_pc.c
	miniat_ports/test_ports.c
	miniat_registers/test_registers.c
)

add_executable(miniat_tests "${SOURCES}")

target_include_directories(miniat_tests PRIVATE
	"${VM_INCLUDE_DIR}"
	"${LUA_INCLUDE_DIR}"
	cutest-1.5
)

target_link_libraries(miniat_tests PRIVATE
	miniat
)

set(TEST_ASMS
	port_input
	timer_time
	ints_high_low
	bus_read
	timer_count
	store_load
	timer_reset
	timer_toggle
	branch_delay
	bus_write
	flush
	watchdog
)

foreach(TEST_FILE ${TEST_ASMS})
	configure_file("${CMAKE_CURRENT_SOURCE_DIR}/asms/${TEST_FILE}.asm" ${TEST_FILE}.asm)
	add_custom_command(
		OUTPUT ${TEST_FILE}.bin
		COMMAND mash ${TEST_FILE}.asm
		DEPENDS
			mash
			${TEST_FILE}.asm
			"${CMAKE_CURRENT_SOURCE_DIR}/asms/${TEST_FILE}.asm"
		BYPRODUCTS ${TEST_FILE}.bin ${TEST_FILE}.asm

		COMMENT "Assembling test: ${TEST_FILE}.asm"
	)
	add_custom_target(assemble_test_${TEST_FILE} ALL
		DEPENDS ${TEST_FILE}.bin
	)
endforeach()

#[[--------------------------------------------------------------------
  TODO: These definitions are left over from the original Scons structure and
  should be reviewed at some point.
--------------------------------------------------------------------]]
target_compile_definitions(miniat_tests PRIVATE
	BIN_OUT_DIR="${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}/test"
	CACHE_DIR="${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}"
)

########################################################################
#  "make install" target
########################################################################

install(TARGETS miniat_tests DESTINATION "${CMAKE_INSTALL_BINDIR}")

foreach(TEST_FILE ${TEST_ASMS})
	install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${TEST_FILE}.bin"
		DESTINATION "${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}/test"
	)
endforeach()
