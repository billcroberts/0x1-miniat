/* Driver template for the LEMON parser generator.
** The author disclaims copyright to this source code.
*/
/* First off, code is included that follows the "include" declaration
** in the input grammar file. */
#include <stdio.h>
#line 1 "mash.y"

	#include <assert.h>
	#include <string.h>
	#include <stdbool.h>

	#include "token.h"
	#include "mash.h"
	#include "symbol_tables.h"
	#include "util.h"
	#include "miniat/miniat.h"
	#include "miniat_defines.h"
	#include "blocks.h"

	/*
	 * This unusual variable is used to push the line number of certain
	 * constant_immediate up the parse tree to the appropriate raw_value.
	 * Basically, it's used at a low Token level to capture the line
	 * number before the rules begin passing only "m_uword"s up.  The
	 * higher rule can then use it to determine the line number.  This
	 * data is really only utilized when generating listings.
	 */
	m_uword most_recent_const_immed_line_num;

	bool current_branch_is_flush = false;

	/* Use a bogus line_num until matching rules for a specific type of code line.
	 *
	 * E.g., alu_code_line
	 *
	 * These lines will always have an instruction Token* from which the correct
	 * line number can be determined.
	 */
	void set_instruction(instruction *inst, m_uword predicate, m_uword hint, m_uword unused,
	                     m_uword opcode, m_uword rA, m_uword rB, m_uword rC, m_uword immed,
	                     int line_num) {

		inst->low.predicate = predicate;
		inst->low.hint = hint;
		inst->low.unused = unused;
		inst->low.opcode = opcode;
		inst->low.rA = rA;
		inst->low.rB = rB;
		inst->low.rC = rC;
		inst->high.u = immed;
		inst->line_num = line_num;

		return;
	}
#line 57 "mash.c"
/* Next is all token values, in a form suitable for use by makeheaders.
** This section will be null unless lemon is run with the -m switch.
*/
/* 
** These constants (all generated automatically by the parser generator)
** specify the various kinds of tokens (terminals) that the parser
** understands. 
**
** Each symbol here is a terminal symbol in the grammar.
*/
/* Make sure the INTERFACE macro is defined.
*/
#ifndef INTERFACE
# define INTERFACE 1
#endif
/* The next thing included is series of defines which control
** various aspects of the generated parser.
**    YYCODETYPE         is the data type used for storing terminal
**                       and nonterminal numbers.  "unsigned char" is
**                       used if there are fewer than 250 terminals
**                       and nonterminals.  "int" is used otherwise.
**    YYNOCODE           is a number of type YYCODETYPE which corresponds
**                       to no legal terminal or nonterminal number.  This
**                       number is used to fill in empty slots of the hash 
**                       table.
**    YYFALLBACK         If defined, this indicates that one or more tokens
**                       have fall-back values which should be used if the
**                       original value of the token will not parse.
**    YYACTIONTYPE       is the data type used for storing terminal
**                       and nonterminal numbers.  "unsigned char" is
**                       used if there are fewer than 250 rules and
**                       states combined.  "int" is used otherwise.
**    ParseTOKENTYPE     is the data type used for minor tokens given 
**                       directly to the parser from the tokenizer.
**    YYMINORTYPE        is the data type used for all minor tokens.
**                       This is typically a union of many types, one of
**                       which is ParseTOKENTYPE.  The entry in the union
**                       for base tokens is called "yy0".
**    YYSTACKDEPTH       is the maximum depth of the parser's stack.  If
**                       zero the stack is dynamically sized using realloc()
**    ParseARG_SDECL     A static variable declaration for the %extra_argument
**    ParseARG_PDECL     A parameter declaration for the %extra_argument
**    ParseARG_STORE     Code to store %extra_argument into yypParser
**    ParseARG_FETCH     Code to extract %extra_argument from yypParser
**    YYNSTATE           the combined number of states.
**    YYNRULE            the number of rules in the grammar
**    YYERRORSYMBOL      is the code number of the error symbol.  If not
**                       defined, then do no error processing.
*/
#define YYCODETYPE unsigned char
#define YYNOCODE 124
#define YYACTIONTYPE unsigned short int
#define ParseTOKENTYPE Token *
typedef union {
  int yyinit;
  ParseTOKENTYPE yy0;
  instruction yy61;
  m_word yy79;
  int yy247;
} YYMINORTYPE;
#ifndef YYSTACKDEPTH
#define YYSTACKDEPTH 100
#endif
#define ParseARG_SDECL
#define ParseARG_PDECL
#define ParseARG_FETCH
#define ParseARG_STORE
#define YYNSTATE 206
#define YYNRULE 128
#define YYERRORSYMBOL 73
#define YYERRSYMDT yy247
#define YY_NO_ACTION      (YYNSTATE+YYNRULE+2)
#define YY_ACCEPT_ACTION  (YYNSTATE+YYNRULE+1)
#define YY_ERROR_ACTION   (YYNSTATE+YYNRULE)

/* The yyzerominor constant is used to initialize instances of
** YYMINORTYPE objects to zero. */
static const YYMINORTYPE yyzerominor = { 0 };

/* Define the yytestcase() macro to be a no-op if is not already defined
** otherwise.
**
** Applications can choose to define yytestcase() in the %include section
** to a macro that can assist in verifying code coverage.  For production
** code the yytestcase() macro should be turned off.  But it is useful
** for testing.
*/
#ifndef yytestcase
# define yytestcase(X)
#endif


/* Next are the tables used to determine what action to take based on the
** current state and lookahead token.  These tables are used to implement
** functions that take a state number and lookahead value and return an
** action integer.  
**
** Suppose the action integer is N.  Then the action is determined as
** follows
**
**   0 <= N < YYNSTATE                  Shift N.  That is, push the lookahead
**                                      token onto the stack and goto state N.
**
**   YYNSTATE <= N < YYNSTATE+YYNRULE   Reduce by rule N-YYNSTATE.
**
**   N == YYNSTATE+YYNRULE              A syntax error has occurred.
**
**   N == YYNSTATE+YYNRULE+1            The parser accepts its input.
**
**   N == YYNSTATE+YYNRULE+2            No such action.  Denotes unused
**                                      slots in the yy_action[] table.
**
** The action table is constructed as a single large table named yy_action[].
** Given state S and lookahead X, the action is computed as
**
**      yy_action[ yy_shift_ofst[S] + X ]
**
** If the index value yy_shift_ofst[S]+X is out of range or if the value
** yy_lookahead[yy_shift_ofst[S]+X] is not equal to X or if yy_shift_ofst[S]
** is equal to YY_SHIFT_USE_DFLT, it means that the action is not in the table
** and that yy_default[S] should be used instead.  
**
** The formula above is for computing the action when the lookahead is
** a terminal symbol.  If the lookahead is a non-terminal (as occurs after
** a reduce action) then the yy_reduce_ofst[] array is used in place of
** the yy_shift_ofst[] array and YY_REDUCE_USE_DFLT is used in place of
** YY_SHIFT_USE_DFLT.
**
** The following are the tables generated in this section:
**
**  yy_action[]        A single table containing all actions.
**  yy_lookahead[]     A table containing the lookahead for each entry in
**                     yy_action.  Used to detect hash collisions.
**  yy_shift_ofst[]    For each state, the offset into yy_action for
**                     shifting terminals.
**  yy_reduce_ofst[]   For each state, the offset into yy_action for
**                     shifting non-terminals after a reduce.
**  yy_default[]       Default action for each state.
*/
#define YY_ACTTAB_COUNT (377)
static const YYACTIONTYPE yy_action[] = {
 /*     0 */   127,  126,  125,  124,  123,  122,  121,  120,  119,  118,
 /*    10 */   205,  153,  152,  151,  150,  149,  148,  147,  146,  145,
 /*    20 */   144,  203,  117,  116,  115,  114,  113,  112,  111,  110,
 /*    30 */   109,  108,  136,  135,  134,  133,  132,  131,  192,  191,
 /*    40 */   196,    4,  104,   59,   58,   57,   56,   55,   52,   34,
 /*    50 */    96,   95,  183,  182,  181,  180,  179,  178,  177,  176,
 /*    60 */   175,  174,  173,  172,  171,  170,  169,  168,   79,  189,
 /*    70 */    19,   74,   30,   29,   26,   25,   24,   23,   22,   10,
 /*    80 */    21,  201,  198,  161,  184,    1,  206,  188,   84,  162,
 /*    90 */    50,  161,   83,   32,   82,  129,  128,  162,   18,  185,
 /*   100 */   200,  199,   94,  160,  160,  194,  193,  194,  193,  166,
 /*   110 */    76,   42,  195,  194,  193,  194,  193,    8,   17,   86,
 /*   120 */   207,   81,   84,  186,  187,   41,   83,   20,   82,   49,
 /*   130 */    33,  156,   18,  185,  200,  199,   94,  106,  105,  195,
 /*   140 */    92,  166,   87,   80,  164,  163,   86,  165,    7,  194,
 /*   150 */   193,  201,  198,  195,  202,  165,   38,  197,  331,  195,
 /*   160 */    86,   46,   92,  156,   93,  166,   86,   45,  166,   75,
 /*   170 */    92,  195,   91,  195,   27,  158,  164,  163,   86,   92,
 /*   180 */    86,   90,  195,   92,  166,   89,   31,  156,  166,   86,
 /*   190 */   156,  197,  331,   71,   44,  195,   35,   43,  195,  141,
 /*   200 */   164,  163,   86,  164,  163,   86,  156,   40,  190,  198,
 /*   210 */   154,   92,   60,   88,  195,  159,   39,  139,  195,  164,
 /*   220 */   163,   86,  161,  164,  163,   86,  166,  161,  162,   92,
 /*   230 */   166,   85,  155,  162,  194,  193,   37,   32,   36,  195,
 /*   240 */    67,   63,   28,  137,  194,  193,   86,  130,  167,  194,
 /*   250 */   193,  107,  157,  103,    9,  102,  195,  166,  101,    6,
 /*   260 */   195,  164,  163,   86,  100,  164,  163,   86,  166,   92,
 /*   270 */   335,   47,    2,   54,    3,   53,  197,  166,   99,  143,
 /*   280 */    51,   77,   11,  160,   16,   72,  165,  195,  166,  142,
 /*   290 */   140,  165,  164,  163,   86,    8,   48,   14,  195,  138,
 /*   300 */    70,   69,   97,  164,  163,   86,  204,  195,   13,    7,
 /*   310 */    98,   62,  164,  163,   86,   66,   65,   68,  195,  200,
 /*   320 */   199,   15,   12,  164,  163,   86,   61,  204,    5,   78,
 /*   330 */    73,   64,  336,  336,  194,  193,  336,  336,  336,  336,
 /*   340 */   336,  336,  336,  336,  336,  336,  336,  336,  336,  336,
 /*   350 */   336,  336,  336,  336,  336,  336,  336,  336,  336,  336,
 /*   360 */   336,  336,  336,  336,  336,  336,  336,  336,  336,  336,
 /*   370 */   336,  336,  336,  336,  336,  336,  197,
};
static const YYCODETYPE yy_lookahead[] = {
 /*     0 */    19,   20,   21,   22,   23,   24,   25,   26,   27,   28,
 /*    10 */     1,   29,   30,   31,   32,   33,   34,   35,   36,   37,
 /*    20 */    38,    1,   41,   42,   43,   44,   45,   46,   47,   48,
 /*    30 */    49,   50,   54,   55,   56,   57,   58,   59,    4,    5,
 /*    40 */    72,   60,   61,   62,   63,   64,   65,   66,   67,   68,
 /*    50 */    69,   70,   89,   90,   91,   92,   93,   94,   95,   96,
 /*    60 */    97,   98,   99,  100,  101,  102,  103,  104,  105,   10,
 /*    70 */    11,  108,  109,  110,   77,   78,   79,   80,   81,   82,
 /*    80 */    83,   84,   85,    7,   87,   88,    0,   10,    2,   13,
 /*    90 */     3,    7,    6,   17,    8,   52,   53,   13,   12,   13,
 /*   100 */    14,   15,   16,   11,   11,   29,   30,   29,   30,   85,
 /*   110 */    18,   18,  115,   29,   30,   29,   30,    9,    7,  122,
 /*   120 */     0,    7,    2,   85,   86,   17,    6,    9,    8,   85,
 /*   130 */    17,  107,   12,   13,   14,   15,   16,  113,  114,  115,
 /*   140 */    73,   85,   75,   85,  120,  121,  122,   71,    9,   29,
 /*   150 */    30,   84,   85,  115,   87,   71,   17,   71,   72,  115,
 /*   160 */   122,   17,   73,  107,   75,   85,  122,   17,   85,  113,
 /*   170 */    73,  115,   75,  115,   17,   40,  120,  121,  122,   73,
 /*   180 */   122,   75,  115,   73,   85,   75,   17,  107,   85,  122,
 /*   190 */   107,   71,   72,  113,   17,  115,  113,   17,  115,   10,
 /*   200 */   120,  121,  122,  120,  121,  122,  107,   17,   84,   85,
 /*   210 */   107,   73,  113,   75,  115,  112,   17,   17,  115,  120,
 /*   220 */   121,  122,    7,  120,  121,  122,   85,    7,   13,   73,
 /*   230 */    85,   75,   17,   13,   29,   30,   17,   17,   17,  115,
 /*   240 */    18,   11,   17,   17,   29,   30,  122,   10,  107,   29,
 /*   250 */    30,   40,  107,   17,   39,   17,  115,   85,   17,   39,
 /*   260 */   115,  120,  121,  122,   17,  120,  121,  122,   85,   73,
 /*   270 */    74,   75,   76,   17,   76,   18,   71,   85,   17,  107,
 /*   280 */    17,  106,   18,   11,  106,  106,   71,  115,   85,  116,
 /*   290 */   107,   71,  120,  121,  122,    9,   51,  106,  115,  107,
 /*   300 */   106,  106,  117,  120,  121,  122,    1,  115,  106,    9,
 /*   310 */   107,  118,  120,  121,  122,  106,  106,  116,  115,   14,
 /*   320 */    15,  115,  106,  120,  121,  122,  119,    1,  111,  106,
 /*   330 */   106,  117,  123,  123,   29,   30,  123,  123,  123,  123,
 /*   340 */   123,  123,  123,  123,  123,  123,  123,  123,  123,  123,
 /*   350 */   123,  123,  123,  123,  123,  123,  123,  123,  123,  123,
 /*   360 */   123,  123,  123,  123,  123,  123,  123,  123,  123,  123,
 /*   370 */   123,  123,  123,  123,  123,  123,   71,
};
#define YY_SHIFT_USE_DFLT (-33)
#define YY_SHIFT_COUNT (93)
#define YY_SHIFT_MIN   (-32)
#define YY_SHIFT_MAX   (326)
static const short yy_shift_ofst[] = {
 /*     0 */   326,  -19,  120,   86,  220,  215,   76,   76,   76,   76,
 /*    10 */   305,   84,   84,   84,   84,   84,   84,  205,   78,   78,
 /*    20 */    78,  326,  326,  326,  326,  326,  326,  -18,  -22,  139,
 /*    30 */   108,   93,   78,   92,  300,  245,  272,  272,  272,  272,
 /*    40 */   272,  272,  286,  272,  272,  272,  272,    9,   43,   59,
 /*    50 */    34,  264,  263,  261,  257,  256,  247,  241,  238,  236,
 /*    60 */   211,  237,  226,  225,  230,  221,  219,  200,  222,  199,
 /*    70 */   190,  189,  180,  177,  169,  135,  157,  150,  144,  113,
 /*    80 */    77,  118,  114,  111,   87,    9,  -32,    9,    9,    9,
 /*    90 */     9,    9,   20,    9,
};
#define YY_REDUCE_USE_DFLT (-38)
#define YY_REDUCE_COUNT (47)
#define YY_REDUCE_MIN   (-37)
#define YY_REDUCE_MAX   (224)
static const short yy_reduce_ofst[] = {
 /*     0 */   196,  -37,   -3,   -3,   24,  103,   99,   83,   80,   56,
 /*    10 */    67,  203,  192,  183,  172,  145,  141,  124,   38,   58,
 /*    20 */    44,  156,  138,  110,  106,   97,   89,  217,  193,  214,
 /*    30 */   201,  224,  206,  223,  185,  207,  216,  210,  209,  202,
 /*    40 */   195,  194,  173,  191,  179,  178,  175,  198,
};
static const YYACTIONTYPE yy_default[] = {
 /*     0 */   218,  334,  234,  234,  331,  331,  331,  331,  331,  331,
 /*    10 */   331,  331,  331,  331,  331,  331,  331,  331,  331,  331,
 /*    20 */   331,  334,  334,  334,  334,  334,  334,  334,  334,  334,
 /*    30 */   334,  255,  283,  255,  334,  302,  255,  255,  255,  255,
 /*    40 */   255,  255,  334,  255,  255,  255,  255,  218,  334,  334,
 /*    50 */   334,  334,  334,  334,  334,  334,  334,  334,  334,  334,
 /*    60 */   334,  334,  334,  334,  334,  334,  334,  334,  334,  334,
 /*    70 */   334,  334,  334,  334,  334,  334,  334,  334,  334,  334,
 /*    80 */   334,  222,  334,  334,  334,  217,  334,  216,  215,  214,
 /*    90 */   213,  212,  334,  211,  233,  319,  318,  317,  316,  315,
 /*   100 */   314,  313,  312,  311,  310,  309,  281,  280,  298,  297,
 /*   110 */   296,  295,  294,  293,  291,  290,  289,  288,  266,  265,
 /*   120 */   264,  263,  262,  261,  260,  259,  258,  257,  301,  300,
 /*   130 */   299,  308,  307,  306,  305,  304,  303,  292,  253,  286,
 /*   140 */   252,  287,  285,  251,  276,  275,  274,  273,  272,  271,
 /*   150 */   270,  269,  268,  267,  279,  278,  284,  282,  277,  256,
 /*   160 */   254,  325,  324,  323,  322,  321,  320,  250,  249,  248,
 /*   170 */   247,  246,  245,  244,  243,  242,  241,  240,  239,  238,
 /*   180 */   237,  236,  235,  232,  228,  226,  328,  225,  224,  223,
 /*   190 */   221,  220,  219,  333,  332,  330,  329,  327,  326,  231,
 /*   200 */   230,  229,  227,  210,  209,  208,
};

/* The next table maps tokens into fallback tokens.  If a construct
** like the following:
** 
**      %fallback ID X Y Z.
**
** appears in the grammar, then ID becomes a fallback token for X, Y,
** and Z.  Whenever one of the tokens X, Y, or Z is input to the parser
** but it does not parse, the type of the token is changed to ID and
** the parse is retried before an error is thrown.
*/
#ifdef YYFALLBACK
static const YYCODETYPE yyFallback[] = {
};
#endif /* YYFALLBACK */

/* The following structure represents a single element of the
** parser's stack.  Information stored includes:
**
**   +  The state number for the parser at this level of the stack.
**
**   +  The value of the token stored at this level of the stack.
**      (In other words, the "major" token.)
**
**   +  The semantic value stored at this level of the stack.  This is
**      the information used by the action routines in the grammar.
**      It is sometimes called the "minor" token.
*/
struct yyStackEntry {
  YYACTIONTYPE stateno;  /* The state-number */
  YYCODETYPE major;      /* The major token value.  This is the code
                         ** number for the token at this stack level */
  YYMINORTYPE minor;     /* The user-supplied minor token value.  This
                         ** is the value of the token  */
};
typedef struct yyStackEntry yyStackEntry;

/* The state of the parser is completely contained in an instance of
** the following structure */
struct yyParser {
  int yyidx;                    /* Index of top element in stack */
#ifdef YYTRACKMAXSTACKDEPTH
  int yyidxMax;                 /* Maximum value of yyidx */
#endif
  int yyerrcnt;                 /* Shifts left before out of the error */
  ParseARG_SDECL                /* A place to hold %extra_argument */
#if YYSTACKDEPTH<=0
  int yystksz;                  /* Current side of the stack */
  yyStackEntry *yystack;        /* The parser's stack */
#else
  yyStackEntry yystack[YYSTACKDEPTH];  /* The parser's stack */
#endif
};
typedef struct yyParser yyParser;

#ifndef NDEBUG
#include <stdio.h>
static FILE *yyTraceFILE = 0;
static char *yyTracePrompt = 0;
#endif /* NDEBUG */

#ifndef NDEBUG
/* 
** Turn parser tracing on by giving a stream to which to write the trace
** and a prompt to preface each trace message.  Tracing is turned off
** by making either argument NULL 
**
** Inputs:
** <ul>
** <li> A FILE* to which trace output should be written.
**      If NULL, then tracing is turned off.
** <li> A prefix string written at the beginning of every
**      line of trace output.  If NULL, then tracing is
**      turned off.
** </ul>
**
** Outputs:
** None.
*/
void ParseTrace(FILE *TraceFILE, char *zTracePrompt){
  yyTraceFILE = TraceFILE;
  yyTracePrompt = zTracePrompt;
  if( yyTraceFILE==0 ) yyTracePrompt = 0;
  else if( yyTracePrompt==0 ) yyTraceFILE = 0;
}
#endif /* NDEBUG */

#ifndef NDEBUG
/* For tracing shifts, the names of all terminals and nonterminals
** are required.  The following table supplies these names */
static const char *const yyTokenName[] = { 
  "$",             "EOL",           "MODE_DIRECTIVE",  "NOP_DELAY",   
  "ON",            "OFF",           "CONST_DIRECTIVE",  "IDENTIFIER",  
  "VAR_DIRECTIVE",  "LEFT_BRACKET",  "RIGHT_BRACKET",  "COMMA",       
  "ADDR_DIRECTIVE",  "LABEL",         "STRING",        "PACKED_STRING",
  "PREDICATE",     "REGISTER",      "EQUALS",        "ADD",         
  "SUB",           "MULT",          "DIV",           "MOD",         
  "AND",           "OR",            "EXOR",          "SHL",         
  "SHR",           "ADD_OPER",      "SUB_OPER",      "MULT_OPER",   
  "DIV_OPER",      "MOD_OPER",      "AND_OPER",      "OR_OPER",     
  "EXOR_OPER",     "SHL_OPER",      "SHR_OPER",      "LEFT_PAREN",  
  "RIGHT_PAREN",   "LOAD",          "RLOAD",         "STOR",        
  "RSTOR",         "BRAE",          "BRANE",         "BRAL",        
  "BRALE",         "BRAG",          "BRAGE",         "COLON",       
  "TRUE",          "FALSE",         "EQUIV",         "NOT_EQUIV",   
  "LESS",          "LESS_OR_EQUIV",  "GREATER",       "GREATER_OR_EQUIV",
  "INT",           "IRET",          "NEG",           "INV",         
  "INC",           "DEC",           "MOVR",          "MOVI",        
  "BRA",           "NOP",           "FLUSH",         "CHARACTER",   
  "NUMBER",        "error",         "mash_program",  "eols",        
  "lines",         "mode_line",     "constant_line",  "variable_line",
  "address_line",  "label_line",    "raw_line",      "code_line",   
  "constant_immediate",  "number",        "address_immediate",  "raw_value",   
  "optional_predicate",  "rest_of_the_code_line",  "complete_code_line",  "alu_code_line",
  "data_code_line",  "control_code_line",  "int_code_line",  "int_return_code_line",
  "neg_code_line",  "inv_code_line",  "inc_code_line",  "dec_code_line",
  "movr_code_line",  "movi_code_line",  "bra_code_line",  "nop_code_line",
  "flush_code_line",  "alu_instruction",  "comma",         "immediate",   
  "data_load_instruction",  "data_stor_instruction",  "control_instruction",  "alu_operator",
  "paren_reg_immed_expr",  "reg_immediate_expr",  "optional_paren_reg_immed_expr",  "add_or_subtract",
  "bracket_reg_immed_expr",  "control_address_expr",  "conditional_oper",  "hint",        
  "label_immediate",  "identifier_immediate",  "optional_sign",
};
#endif /* NDEBUG */

#ifndef NDEBUG
/* For tracing reduce actions, the names of all rules are required.
*/
static const char *const yyRuleName[] = {
 /*   0 */ "mash_program ::= eols lines",
 /*   1 */ "mash_program ::= lines",
 /*   2 */ "eols ::= eols EOL",
 /*   3 */ "eols ::= EOL",
 /*   4 */ "eols ::= error EOL",
 /*   5 */ "lines ::= lines mode_line eols",
 /*   6 */ "lines ::= lines constant_line eols",
 /*   7 */ "lines ::= lines variable_line eols",
 /*   8 */ "lines ::= lines address_line eols",
 /*   9 */ "lines ::= lines label_line eols",
 /*  10 */ "lines ::= lines raw_line eols",
 /*  11 */ "lines ::= lines code_line eols",
 /*  12 */ "lines ::=",
 /*  13 */ "mode_line ::= MODE_DIRECTIVE NOP_DELAY ON",
 /*  14 */ "mode_line ::= MODE_DIRECTIVE NOP_DELAY OFF",
 /*  15 */ "constant_line ::= CONST_DIRECTIVE IDENTIFIER constant_immediate",
 /*  16 */ "variable_line ::= VAR_DIRECTIVE IDENTIFIER",
 /*  17 */ "variable_line ::= VAR_DIRECTIVE IDENTIFIER LEFT_BRACKET number RIGHT_BRACKET",
 /*  18 */ "variable_line ::= VAR_DIRECTIVE IDENTIFIER LEFT_BRACKET number COMMA number RIGHT_BRACKET",
 /*  19 */ "address_line ::= ADDR_DIRECTIVE address_immediate",
 /*  20 */ "label_line ::= LABEL",
 /*  21 */ "raw_line ::= raw_line raw_value",
 /*  22 */ "raw_line ::= raw_value",
 /*  23 */ "raw_value ::= constant_immediate",
 /*  24 */ "raw_value ::= STRING",
 /*  25 */ "raw_value ::= PACKED_STRING",
 /*  26 */ "code_line ::= optional_predicate rest_of_the_code_line",
 /*  27 */ "optional_predicate ::= PREDICATE",
 /*  28 */ "optional_predicate ::=",
 /*  29 */ "rest_of_the_code_line ::= complete_code_line",
 /*  30 */ "rest_of_the_code_line ::= alu_code_line",
 /*  31 */ "rest_of_the_code_line ::= data_code_line",
 /*  32 */ "rest_of_the_code_line ::= control_code_line",
 /*  33 */ "rest_of_the_code_line ::= int_code_line",
 /*  34 */ "rest_of_the_code_line ::= int_return_code_line",
 /*  35 */ "rest_of_the_code_line ::= neg_code_line",
 /*  36 */ "rest_of_the_code_line ::= inv_code_line",
 /*  37 */ "rest_of_the_code_line ::= inc_code_line",
 /*  38 */ "rest_of_the_code_line ::= dec_code_line",
 /*  39 */ "rest_of_the_code_line ::= movr_code_line",
 /*  40 */ "rest_of_the_code_line ::= movi_code_line",
 /*  41 */ "rest_of_the_code_line ::= bra_code_line",
 /*  42 */ "rest_of_the_code_line ::= nop_code_line",
 /*  43 */ "rest_of_the_code_line ::= flush_code_line",
 /*  44 */ "complete_code_line ::= alu_instruction REGISTER comma REGISTER comma REGISTER comma immediate",
 /*  45 */ "complete_code_line ::= data_load_instruction REGISTER comma REGISTER comma REGISTER comma immediate",
 /*  46 */ "complete_code_line ::= data_stor_instruction REGISTER comma REGISTER comma REGISTER comma immediate",
 /*  47 */ "complete_code_line ::= control_instruction REGISTER comma REGISTER comma REGISTER comma immediate",
 /*  48 */ "comma ::= COMMA",
 /*  49 */ "comma ::=",
 /*  50 */ "alu_code_line ::= alu_instruction REGISTER EQUALS REGISTER alu_operator paren_reg_immed_expr",
 /*  51 */ "alu_instruction ::= ADD",
 /*  52 */ "alu_instruction ::= SUB",
 /*  53 */ "alu_instruction ::= MULT",
 /*  54 */ "alu_instruction ::= DIV",
 /*  55 */ "alu_instruction ::= MOD",
 /*  56 */ "alu_instruction ::= AND",
 /*  57 */ "alu_instruction ::= OR",
 /*  58 */ "alu_instruction ::= EXOR",
 /*  59 */ "alu_instruction ::= SHL",
 /*  60 */ "alu_instruction ::= SHR",
 /*  61 */ "alu_operator ::= ADD_OPER",
 /*  62 */ "alu_operator ::= SUB_OPER",
 /*  63 */ "alu_operator ::= MULT_OPER",
 /*  64 */ "alu_operator ::= DIV_OPER",
 /*  65 */ "alu_operator ::= MOD_OPER",
 /*  66 */ "alu_operator ::= AND_OPER",
 /*  67 */ "alu_operator ::= OR_OPER",
 /*  68 */ "alu_operator ::= EXOR_OPER",
 /*  69 */ "alu_operator ::= SHL_OPER",
 /*  70 */ "alu_operator ::= SHR_OPER",
 /*  71 */ "paren_reg_immed_expr ::= LEFT_PAREN reg_immediate_expr RIGHT_PAREN",
 /*  72 */ "paren_reg_immed_expr ::= REGISTER",
 /*  73 */ "paren_reg_immed_expr ::= immediate",
 /*  74 */ "optional_paren_reg_immed_expr ::= LEFT_PAREN reg_immediate_expr RIGHT_PAREN",
 /*  75 */ "optional_paren_reg_immed_expr ::= reg_immediate_expr",
 /*  76 */ "reg_immediate_expr ::= REGISTER add_or_subtract immediate",
 /*  77 */ "reg_immediate_expr ::= REGISTER",
 /*  78 */ "reg_immediate_expr ::= immediate",
 /*  79 */ "data_code_line ::= data_load_instruction REGISTER EQUALS bracket_reg_immed_expr",
 /*  80 */ "data_code_line ::= data_stor_instruction bracket_reg_immed_expr EQUALS REGISTER",
 /*  81 */ "bracket_reg_immed_expr ::= LEFT_BRACKET reg_immediate_expr RIGHT_BRACKET",
 /*  82 */ "data_load_instruction ::= LOAD",
 /*  83 */ "data_load_instruction ::= RLOAD",
 /*  84 */ "data_stor_instruction ::= STOR",
 /*  85 */ "data_stor_instruction ::= RSTOR",
 /*  86 */ "control_code_line ::= control_instruction control_address_expr COMMA REGISTER conditional_oper REGISTER",
 /*  87 */ "control_instruction ::= BRAE",
 /*  88 */ "control_instruction ::= BRANE",
 /*  89 */ "control_instruction ::= BRAL",
 /*  90 */ "control_instruction ::= BRALE",
 /*  91 */ "control_instruction ::= BRAG",
 /*  92 */ "control_instruction ::= BRAGE",
 /*  93 */ "control_address_expr ::= LEFT_BRACKET reg_immediate_expr hint RIGHT_BRACKET",
 /*  94 */ "hint ::= COLON TRUE",
 /*  95 */ "hint ::= COLON FALSE",
 /*  96 */ "hint ::=",
 /*  97 */ "conditional_oper ::= EQUIV",
 /*  98 */ "conditional_oper ::= NOT_EQUIV",
 /*  99 */ "conditional_oper ::= LESS",
 /* 100 */ "conditional_oper ::= LESS_OR_EQUIV",
 /* 101 */ "conditional_oper ::= GREATER",
 /* 102 */ "conditional_oper ::= GREATER_OR_EQUIV",
 /* 103 */ "int_code_line ::= INT optional_paren_reg_immed_expr",
 /* 104 */ "int_return_code_line ::= IRET",
 /* 105 */ "neg_code_line ::= NEG REGISTER",
 /* 106 */ "inv_code_line ::= INV REGISTER",
 /* 107 */ "inc_code_line ::= INC REGISTER",
 /* 108 */ "dec_code_line ::= DEC REGISTER",
 /* 109 */ "movr_code_line ::= MOVR REGISTER EQUALS REGISTER",
 /* 110 */ "movi_code_line ::= MOVI REGISTER EQUALS immediate",
 /* 111 */ "bra_code_line ::= BRA control_address_expr",
 /* 112 */ "nop_code_line ::= NOP",
 /* 113 */ "flush_code_line ::= FLUSH",
 /* 114 */ "immediate ::= number",
 /* 115 */ "immediate ::= CHARACTER",
 /* 116 */ "immediate ::= label_immediate",
 /* 117 */ "immediate ::= identifier_immediate",
 /* 118 */ "label_immediate ::= LABEL",
 /* 119 */ "identifier_immediate ::= IDENTIFIER",
 /* 120 */ "constant_immediate ::= number",
 /* 121 */ "constant_immediate ::= CHARACTER",
 /* 122 */ "address_immediate ::= number",
 /* 123 */ "number ::= optional_sign NUMBER",
 /* 124 */ "optional_sign ::= add_or_subtract",
 /* 125 */ "optional_sign ::=",
 /* 126 */ "add_or_subtract ::= ADD_OPER",
 /* 127 */ "add_or_subtract ::= SUB_OPER",
};
#endif /* NDEBUG */


#if YYSTACKDEPTH<=0
/*
** Try to increase the size of the parser stack.
*/
static void yyGrowStack(yyParser *p){
  int newSize;
  yyStackEntry *pNew;

  newSize = p->yystksz*2 + 100;
  pNew = realloc(p->yystack, newSize*sizeof(pNew[0]));
  if( pNew ){
    p->yystack = pNew;
    p->yystksz = newSize;
#ifndef NDEBUG
    if( yyTraceFILE ){
      fprintf(yyTraceFILE,"%sStack grows to %d entries!\n",
              yyTracePrompt, p->yystksz);
    }
#endif
  }
}
#endif

/* 
** This function allocates a new parser.
** The only argument is a pointer to a function which works like
** malloc.
**
** Inputs:
** A pointer to the function used to allocate memory.
**
** Outputs:
** A pointer to a parser.  This pointer is used in subsequent calls
** to Parse and ParseFree.
*/
void *ParseAlloc(void *(*mallocProc)(size_t)){
  yyParser *pParser;
  pParser = (yyParser*)(*mallocProc)( (size_t)sizeof(yyParser) );
  if( pParser ){
    pParser->yyidx = -1;
#ifdef YYTRACKMAXSTACKDEPTH
    pParser->yyidxMax = 0;
#endif
#if YYSTACKDEPTH<=0
    pParser->yystack = NULL;
    pParser->yystksz = 0;
    yyGrowStack(pParser);
#endif
  }
  return pParser;
}

/* The following function deletes the value associated with a
** symbol.  The symbol can be either a terminal or nonterminal.
** "yymajor" is the symbol code, and "yypminor" is a pointer to
** the value.
*/
static void yy_destructor(
  yyParser *yypParser,    /* The parser */
  YYCODETYPE yymajor,     /* Type code for object to destroy */
  YYMINORTYPE *yypminor   /* The object to be destroyed */
){
  ParseARG_FETCH;
  switch( yymajor ){
    /* Here is inserted the actions which take place when a
    ** terminal or non-terminal is destroyed.  This can happen
    ** when the symbol is popped from the stack during a
    ** reduce or during error processing or when a parser is 
    ** being destroyed before it is finished parsing.
    **
    ** Note: during a reduce, the only symbols destroyed are those
    ** which appear on the RHS of the rule, but which are not used
    ** inside the C code.
    */
    default:  break;   /* If no destructor action specified: do nothing */
  }
}

/*
** Pop the parser's stack once.
**
** If there is a destructor routine associated with the token which
** is popped from the stack, then call it.
**
** Return the major token number for the symbol popped.
*/
static int yy_pop_parser_stack(yyParser *pParser){
  YYCODETYPE yymajor;
  yyStackEntry *yytos = &pParser->yystack[pParser->yyidx];

  if( pParser->yyidx<0 ) return 0;
#ifndef NDEBUG
  if( yyTraceFILE && pParser->yyidx>=0 ){
    fprintf(yyTraceFILE,"%sPopping %s\n",
      yyTracePrompt,
      yyTokenName[yytos->major]);
  }
#endif
  yymajor = yytos->major;
  yy_destructor(pParser, yymajor, &yytos->minor);
  pParser->yyidx--;
  return yymajor;
}

/* 
** Deallocate and destroy a parser.  Destructors are all called for
** all stack elements before shutting the parser down.
**
** Inputs:
** <ul>
** <li>  A pointer to the parser.  This should be a pointer
**       obtained from ParseAlloc.
** <li>  A pointer to a function used to reclaim memory obtained
**       from malloc.
** </ul>
*/
void ParseFree(
  void *p,                    /* The parser to be deleted */
  void (*freeProc)(void*)     /* Function used to reclaim memory */
){
  yyParser *pParser = (yyParser*)p;
  if( pParser==0 ) return;
  while( pParser->yyidx>=0 ) yy_pop_parser_stack(pParser);
#if YYSTACKDEPTH<=0
  free(pParser->yystack);
#endif
  (*freeProc)((void*)pParser);
}

/*
** Return the peak depth of the stack for a parser.
*/
#ifdef YYTRACKMAXSTACKDEPTH
int ParseStackPeak(void *p){
  yyParser *pParser = (yyParser*)p;
  return pParser->yyidxMax;
}
#endif

/*
** Find the appropriate action for a parser given the terminal
** look-ahead token iLookAhead.
**
** If the look-ahead token is YYNOCODE, then check to see if the action is
** independent of the look-ahead.  If it is, return the action, otherwise
** return YY_NO_ACTION.
*/
static int yy_find_shift_action(
  yyParser *pParser,        /* The parser */
  YYCODETYPE iLookAhead     /* The look-ahead token */
){
  int i;
  int stateno = pParser->yystack[pParser->yyidx].stateno;
 
  if( stateno>YY_SHIFT_COUNT
   || (i = yy_shift_ofst[stateno])==YY_SHIFT_USE_DFLT ){
    return yy_default[stateno];
  }
  assert( iLookAhead!=YYNOCODE );
  i += iLookAhead;
  if( i<0 || i>=YY_ACTTAB_COUNT || yy_lookahead[i]!=iLookAhead ){
    if( iLookAhead>0 ){
#ifdef YYFALLBACK
      YYCODETYPE iFallback;            /* Fallback token */
      if( iLookAhead<sizeof(yyFallback)/sizeof(yyFallback[0])
             && (iFallback = yyFallback[iLookAhead])!=0 ){
#ifndef NDEBUG
        if( yyTraceFILE ){
          fprintf(yyTraceFILE, "%sFALLBACK %s => %s\n",
             yyTracePrompt, yyTokenName[iLookAhead], yyTokenName[iFallback]);
        }
#endif
        return yy_find_shift_action(pParser, iFallback);
      }
#endif
#ifdef YYWILDCARD
      {
        int j = i - iLookAhead + YYWILDCARD;
        if( 
#if YY_SHIFT_MIN+YYWILDCARD<0
          j>=0 &&
#endif
#if YY_SHIFT_MAX+YYWILDCARD>=YY_ACTTAB_COUNT
          j<YY_ACTTAB_COUNT &&
#endif
          yy_lookahead[j]==YYWILDCARD
        ){
#ifndef NDEBUG
          if( yyTraceFILE ){
            fprintf(yyTraceFILE, "%sWILDCARD %s => %s\n",
               yyTracePrompt, yyTokenName[iLookAhead], yyTokenName[YYWILDCARD]);
          }
#endif /* NDEBUG */
          return yy_action[j];
        }
      }
#endif /* YYWILDCARD */
    }
    return yy_default[stateno];
  }else{
    return yy_action[i];
  }
}

/*
** Find the appropriate action for a parser given the non-terminal
** look-ahead token iLookAhead.
**
** If the look-ahead token is YYNOCODE, then check to see if the action is
** independent of the look-ahead.  If it is, return the action, otherwise
** return YY_NO_ACTION.
*/
static int yy_find_reduce_action(
  int stateno,              /* Current state number */
  YYCODETYPE iLookAhead     /* The look-ahead token */
){
  int i;
#ifdef YYERRORSYMBOL
  if( stateno>YY_REDUCE_COUNT ){
    return yy_default[stateno];
  }
#else
  assert( stateno<=YY_REDUCE_COUNT );
#endif
  i = yy_reduce_ofst[stateno];
  assert( i!=YY_REDUCE_USE_DFLT );
  assert( iLookAhead!=YYNOCODE );
  i += iLookAhead;
#ifdef YYERRORSYMBOL
  if( i<0 || i>=YY_ACTTAB_COUNT || yy_lookahead[i]!=iLookAhead ){
    return yy_default[stateno];
  }
#else
  assert( i>=0 && i<YY_ACTTAB_COUNT );
  assert( yy_lookahead[i]==iLookAhead );
#endif
  return yy_action[i];
}

/*
** The following routine is called if the stack overflows.
*/
static void yyStackOverflow(yyParser *yypParser, YYMINORTYPE *yypMinor){
   ParseARG_FETCH;
   yypParser->yyidx--;
#ifndef NDEBUG
   if( yyTraceFILE ){
     fprintf(yyTraceFILE,"%sStack Overflow!\n",yyTracePrompt);
   }
#endif
   while( yypParser->yyidx>=0 ) yy_pop_parser_stack(yypParser);
   /* Here code is inserted which will execute if the parser
   ** stack every overflows */
   ParseARG_STORE; /* Suppress warning about unused %extra_argument var */
}

/*
** Perform a shift action.
*/
static void yy_shift(
  yyParser *yypParser,          /* The parser to be shifted */
  int yyNewState,               /* The new state to shift in */
  int yyMajor,                  /* The major token to shift in */
  YYMINORTYPE *yypMinor         /* Pointer to the minor token to shift in */
){
  yyStackEntry *yytos;
  yypParser->yyidx++;
#ifdef YYTRACKMAXSTACKDEPTH
  if( yypParser->yyidx>yypParser->yyidxMax ){
    yypParser->yyidxMax = yypParser->yyidx;
  }
#endif
#if YYSTACKDEPTH>0 
  if( yypParser->yyidx>=YYSTACKDEPTH ){
    yyStackOverflow(yypParser, yypMinor);
    return;
  }
#else
  if( yypParser->yyidx>=yypParser->yystksz ){
    yyGrowStack(yypParser);
    if( yypParser->yyidx>=yypParser->yystksz ){
      yyStackOverflow(yypParser, yypMinor);
      return;
    }
  }
#endif
  yytos = &yypParser->yystack[yypParser->yyidx];
  yytos->stateno = (YYACTIONTYPE)yyNewState;
  yytos->major = (YYCODETYPE)yyMajor;
  yytos->minor = *yypMinor;
#ifndef NDEBUG
  if( yyTraceFILE && yypParser->yyidx>0 ){
    int i;
    fprintf(yyTraceFILE,"%sShift %d\n",yyTracePrompt,yyNewState);
    fprintf(yyTraceFILE,"%sStack:",yyTracePrompt);
    for(i=1; i<=yypParser->yyidx; i++)
      fprintf(yyTraceFILE," %s",yyTokenName[yypParser->yystack[i].major]);
    fprintf(yyTraceFILE,"\n");
  }
#endif
}

/* The following table contains information about every rule that
** is used during the reduce.
*/
static const struct {
  YYCODETYPE lhs;         /* Symbol on the left-hand side of the rule */
  unsigned char nrhs;     /* Number of right-hand side symbols in the rule */
} yyRuleInfo[] = {
  { 74, 2 },
  { 74, 1 },
  { 75, 2 },
  { 75, 1 },
  { 75, 2 },
  { 76, 3 },
  { 76, 3 },
  { 76, 3 },
  { 76, 3 },
  { 76, 3 },
  { 76, 3 },
  { 76, 3 },
  { 76, 0 },
  { 77, 3 },
  { 77, 3 },
  { 78, 3 },
  { 79, 2 },
  { 79, 5 },
  { 79, 7 },
  { 80, 2 },
  { 81, 1 },
  { 82, 2 },
  { 82, 1 },
  { 87, 1 },
  { 87, 1 },
  { 87, 1 },
  { 83, 2 },
  { 88, 1 },
  { 88, 0 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 89, 1 },
  { 90, 8 },
  { 90, 8 },
  { 90, 8 },
  { 90, 8 },
  { 106, 1 },
  { 106, 0 },
  { 91, 6 },
  { 105, 1 },
  { 105, 1 },
  { 105, 1 },
  { 105, 1 },
  { 105, 1 },
  { 105, 1 },
  { 105, 1 },
  { 105, 1 },
  { 105, 1 },
  { 105, 1 },
  { 111, 1 },
  { 111, 1 },
  { 111, 1 },
  { 111, 1 },
  { 111, 1 },
  { 111, 1 },
  { 111, 1 },
  { 111, 1 },
  { 111, 1 },
  { 111, 1 },
  { 112, 3 },
  { 112, 1 },
  { 112, 1 },
  { 114, 3 },
  { 114, 1 },
  { 113, 3 },
  { 113, 1 },
  { 113, 1 },
  { 92, 4 },
  { 92, 4 },
  { 116, 3 },
  { 108, 1 },
  { 108, 1 },
  { 109, 1 },
  { 109, 1 },
  { 93, 6 },
  { 110, 1 },
  { 110, 1 },
  { 110, 1 },
  { 110, 1 },
  { 110, 1 },
  { 110, 1 },
  { 117, 4 },
  { 119, 2 },
  { 119, 2 },
  { 119, 0 },
  { 118, 1 },
  { 118, 1 },
  { 118, 1 },
  { 118, 1 },
  { 118, 1 },
  { 118, 1 },
  { 94, 2 },
  { 95, 1 },
  { 96, 2 },
  { 97, 2 },
  { 98, 2 },
  { 99, 2 },
  { 100, 4 },
  { 101, 4 },
  { 102, 2 },
  { 103, 1 },
  { 104, 1 },
  { 107, 1 },
  { 107, 1 },
  { 107, 1 },
  { 107, 1 },
  { 120, 1 },
  { 121, 1 },
  { 84, 1 },
  { 84, 1 },
  { 86, 1 },
  { 85, 2 },
  { 122, 1 },
  { 122, 0 },
  { 115, 1 },
  { 115, 1 },
};

static void yy_accept(yyParser*);  /* Forward Declaration */

/*
** Perform a reduce action and the shift that must immediately
** follow the reduce.
*/
static void yy_reduce(
  yyParser *yypParser,         /* The parser */
  int yyruleno                 /* Number of the rule by which to reduce */
){
  int yygoto;                     /* The next state */
  int yyact;                      /* The next action */
  YYMINORTYPE yygotominor;        /* The LHS of the rule reduced */
  yyStackEntry *yymsp;            /* The top of the parser's stack */
  int yysize;                     /* Amount to pop the stack */
  ParseARG_FETCH;
  yymsp = &yypParser->yystack[yypParser->yyidx];
#ifndef NDEBUG
  if( yyTraceFILE && yyruleno>=0 
        && yyruleno<(int)(sizeof(yyRuleName)/sizeof(yyRuleName[0])) ){
    fprintf(yyTraceFILE, "%sReduce [%s].\n", yyTracePrompt,
      yyRuleName[yyruleno]);
  }
#endif /* NDEBUG */

  /* Silence complaints from purify about yygotominor being uninitialized
  ** in some cases when it is copied into the stack after the following
  ** switch.  yygotominor is uninitialized when a rule reduces that does
  ** not set the value of its left-hand side nonterminal.  Leaving the
  ** value of the nonterminal uninitialized is utterly harmless as long
  ** as the value is never used.  So really the only thing this code
  ** accomplishes is to quieten purify.  
  **
  ** 2007-01-16:  The wireshark project (www.wireshark.org) reports that
  ** without this code, their parser segfaults.  I'm not sure what there
  ** parser is doing to make this happen.  This is the second bug report
  ** from wireshark this week.  Clearly they are stressing Lemon in ways
  ** that it has not been previously stressed...  (SQLite ticket #2172)
  */
  /*memset(&yygotominor, 0, sizeof(yygotominor));*/
  yygotominor = yyzerominor;


  switch( yyruleno ){
  /* Beginning here are the reduction cases.  A typical example
  ** follows:
  **   case 0:
  **  #line <lineno> <grammarfile>
  **     { ... }           // User supplied code
  **  #line <lineno> <thisfile>
  **     break;
  */
      case 8: /* lines ::= lines address_line eols */
      case 9: /* lines ::= lines label_line eols */ yytestcase(yyruleno==9);
      case 10: /* lines ::= lines raw_line eols */ yytestcase(yyruleno==10);
#line 78 "mash.y"
{
		inline_variables = true;
}
#line 1087 "mash.c"
        break;
      case 11: /* lines ::= lines code_line eols */
#line 87 "mash.y"
{
		inline_variables = true;
		address.u += 2;
}
#line 1095 "mash.c"
        break;
      case 13: /* mode_line ::= MODE_DIRECTIVE NOP_DELAY ON */
#line 93 "mash.y"
{
	mode_nop_delay = true;
}
#line 1102 "mash.c"
        break;
      case 14: /* mode_line ::= MODE_DIRECTIVE NOP_DELAY OFF */
#line 96 "mash.y"
{
	mode_nop_delay = false;
}
#line 1109 "mash.c"
        break;
      case 15: /* constant_line ::= CONST_DIRECTIVE IDENTIFIER constant_immediate */
#line 100 "mash.y"
{
	if(pass == 1) {
		symbol *sym;
		if(!symbol_table_search(variables, yymsp[-1].minor.yy0->token_str)) {
			sym = symbol_table_add(constants, yymsp[-1].minor.yy0->token_str, yymsp[0].minor.yy79);
			if(!sym) {
				sym = symbol_table_search(constants, yymsp[-1].minor.yy0->token_str);
				if(sym) {
					STAGE_ERROR(yymsp[-1].minor.yy0->line_num, "The constant, \"%s\", is already defined as 0x%08X", yymsp[-1].minor.yy0->token_str, sym->value.u);
				}
				else {
					IMPOSSIBLE("Crepes be burned to William!!!");
				}
			}
		}
		else {
			STAGE_ERROR(yymsp[-1].minor.yy0->line_num, "The identifier \"%s\" has already been defined as a variable", yymsp[-1].minor.yy0->token_str);
		}
	}
}
#line 1133 "mash.c"
        break;
      case 16: /* variable_line ::= VAR_DIRECTIVE IDENTIFIER */
#line 121 "mash.y"
{
	if(pass == 1) {
		m_uword *addr;
		symbol *sym;
		if(!symbol_table_search(constants, yymsp[0].minor.yy0->token_str)) {
			addr = (inline_variables ? &address.u : &variable_address.u);
			sym = symbol_table_add(variables, yymsp[0].minor.yy0->token_str, (m_word)*addr);
			if(sym != NULL) {
				(*addr)++;
			}
			else {
				sym = symbol_table_search(variables, yymsp[0].minor.yy0->token_str);
				if(sym) {
					STAGE_ERROR(yymsp[0].minor.yy0->line_num, "The variable, \"%s\", is already defined at address 0x%04X", yymsp[0].minor.yy0->token_str, sym->value.u);
				}
				else {
					IMPOSSIBLE("There really is a Wookalar!!!");
				}
			}
		}
		else {
			STAGE_ERROR(yymsp[0].minor.yy0->line_num, "The identifier \"%s\" has already been defined as a constant", yymsp[0].minor.yy0->token_str);
		}
	}
}
#line 1162 "mash.c"
        break;
      case 17: /* variable_line ::= VAR_DIRECTIVE IDENTIFIER LEFT_BRACKET number RIGHT_BRACKET */
#line 146 "mash.y"
{
	if(pass == 1) {
		m_uword *addr;
		symbol *sym;
		if(!symbol_table_search(constants, yymsp[-3].minor.yy0->token_str)) {
			addr = (inline_variables ? &address.u : &variable_address.u);
			sym = symbol_table_add(variables, yymsp[-3].minor.yy0->token_str, (m_word)*addr);
			if(sym != NULL) {
				*addr += yymsp[-1].minor.yy79.u;
			}
			else {
				sym = symbol_table_search(variables, yymsp[-3].minor.yy0->token_str);
				if(sym) {
					STAGE_ERROR(yymsp[-3].minor.yy0->line_num, "The variable, \"%s\", is already defined at address 0x%04X", yymsp[-3].minor.yy0->token_str, sym->value.u);
				}
				else {
					IMPOSSIBLE("Stew kidneys with beans.");
				}
			}
		}
		else {
			STAGE_ERROR(yymsp[-3].minor.yy0->line_num, "The identifier \"%s\" has already been defined as a constant", yymsp[-3].minor.yy0->token_str);
		}
	}
}
#line 1191 "mash.c"
        break;
      case 18: /* variable_line ::= VAR_DIRECTIVE IDENTIFIER LEFT_BRACKET number COMMA number RIGHT_BRACKET */
#line 171 "mash.y"
{
	if(pass == 1) {
		if(!symbol_table_search(constants, yymsp[-5].minor.yy0->token_str)) {
			if(yymsp[-3].minor.yy79.s > yymsp[-1].minor.yy79.s) {
				char s[] = "The variable array \"%s\" requires its lowest index be at least as low as it's highest";
				STAGE_ERROR(yymsp[-5].minor.yy0->line_num, s, yymsp[-5].minor.yy0->token_str);
			}
			else {
				m_word base;
				m_word size;
				m_uword *addr;
				symbol *sym;
				addr = (inline_variables ? &address.u : &variable_address.u);
				size.s = yymsp[-1].minor.yy79.s - yymsp[-3].minor.yy79.s + 1;
				base.s = *addr - yymsp[-3].minor.yy79.s;
				sym = symbol_table_add(variables, yymsp[-5].minor.yy0->token_str, base);
				if(sym != NULL) {
					*addr += size.s;
				}
				else {
					sym = symbol_table_search(variables, yymsp[-5].minor.yy0->token_str);
					if(sym) {
						STAGE_ERROR(yymsp[-5].minor.yy0->line_num, "The variable, \"%s\", is already defined at address 0x%04X", yymsp[-5].minor.yy0->token_str, sym->value.u);
					}
					else {
						IMPOSSIBLE("Sew buttons on my underwear.");
					}
				}
			}
		}
		else {
			STAGE_ERROR(yymsp[-5].minor.yy0->line_num, "The identifier \"%s\" has already been defined as a constant", yymsp[-5].minor.yy0->token_str);
		}
	}
}
#line 1230 "mash.c"
        break;
      case 19: /* address_line ::= ADDR_DIRECTIVE address_immediate */
#line 207 "mash.y"
{
	address = yymsp[0].minor.yy79;
	if(pass == 2) {
		block_new(address.u);
	}
}
#line 1240 "mash.c"
        break;
      case 20: /* label_line ::= LABEL */
#line 214 "mash.y"
{
	if(pass == 1) {
		symbol *sym = symbol_table_add(labels, yymsp[0].minor.yy0->token_str, address);
		if(sym == NULL) {
			sym = symbol_table_search(labels, yymsp[0].minor.yy0->token_str);
			if(sym) {
				STAGE_ERROR(yymsp[0].minor.yy0->line_num, "The label, \"%s\", is already defined for address 0x%04X", yymsp[0].minor.yy0->token_str, sym->value.u);
			}
			else {
				IMPOSSIBLE("Grease the pig and send it home.");
			}
		}
	}
}
#line 1258 "mash.c"
        break;
      case 23: /* raw_value ::= constant_immediate */
#line 232 "mash.y"
{
	if(pass == 2) {
		block_new_word(most_recent_const_immed_line_num, yymsp[0].minor.yy79.u);
	}
	address.u++;
}
#line 1268 "mash.c"
        break;
      case 24: /* raw_value ::= STRING */
#line 238 "mash.y"
{
	if(pass == 2) {
		int i;
		for(i = 0; i < strlen(yymsp[0].minor.yy0->token_str) + 1; i++) {
			block_new_word(yymsp[0].minor.yy0->line_num, yymsp[0].minor.yy0->token_str[i]);
		}
	}
	address.u += strlen(yymsp[0].minor.yy0->token_str) + 1;
}
#line 1281 "mash.c"
        break;
      case 25: /* raw_value ::= PACKED_STRING */
#line 247 "mash.y"
{
	if(pass == 2) {
		m_uword word;
		char * s = yymsp[0].minor.yy0->token_str;
		int len = strlen(s) + 1;

		/*
		 * first do whole words
		 */
		while(len >= 4) {
			int i;
			word = 0;
			for(i = 3; i >=0; i--) {
				word <<= 8;
				word |= s[i] & ~(~0U << 8);
			}
			block_new_word(yymsp[0].minor.yy0->line_num, word);
			len -= 4;
			s += 4;
		}

		/*
		 * finish with partial word
		 */
		if(len > 0) {
			int i;
			word = 0;
			for(i = len - 1; i >=0; i--) {
				word <<= 8;
				word |= s[i] & ~(~0U << 8);
			}
			block_new_word(yymsp[0].minor.yy0->line_num, word);
		}
	}
	address.u += (strlen(yymsp[0].minor.yy0->token_str) + 4) / 4;
}
#line 1321 "mash.c"
        break;
      case 26: /* code_line ::= optional_predicate rest_of_the_code_line */
#line 285 "mash.y"
{
	if(yymsp[-1].minor.yy0) {
		yymsp[0].minor.yy61.low.predicate = 1;
	}
	if(pass == 2) {
		int i;
		block_new_word(yymsp[0].minor.yy61.line_num, yymsp[0].minor.yy61.low.all);
		block_new_word(yymsp[0].minor.yy61.line_num, yymsp[0].minor.yy61.high.u);
		if(mode_nop_delay) {
			/* Add 3 NOPs (6 words of 0) to fill delay slots */
			switch(yymsp[0].minor.yy61.low.opcode) {
			case M_BRANE:
				if(current_branch_is_flush) {
					break;
				}
			case M_BRAE:
			case M_BRAL:
			case M_BRALE:
			case M_BRAG:
			case M_BRAGE:
			case M_INT:
			case M_IRET:
				for(i = 0; i < 6; i++) {
					block_new_word(yymsp[0].minor.yy61.line_num, 0);
				}
				break;
			default:
				break;
			}
		}
	}
	if(mode_nop_delay) {
		/* Add 3 NOPs (6 words of 0) to fill delay slots */
		switch(yymsp[0].minor.yy61.low.opcode) {
		case M_BRANE:
			if(current_branch_is_flush) {
				break;
			}
		case M_BRAE:
		case M_BRAL:
		case M_BRALE:
		case M_BRAG:
		case M_BRAGE:
		case M_INT:
		case M_IRET:
			address.u += 6;
			break;
		default:
			break;
		}
	}
	current_branch_is_flush = false;
}
#line 1378 "mash.c"
        break;
      case 27: /* optional_predicate ::= PREDICATE */
      case 61: /* alu_operator ::= ADD_OPER */ yytestcase(yyruleno==61);
      case 62: /* alu_operator ::= SUB_OPER */ yytestcase(yyruleno==62);
      case 63: /* alu_operator ::= MULT_OPER */ yytestcase(yyruleno==63);
      case 64: /* alu_operator ::= DIV_OPER */ yytestcase(yyruleno==64);
      case 65: /* alu_operator ::= MOD_OPER */ yytestcase(yyruleno==65);
      case 66: /* alu_operator ::= AND_OPER */ yytestcase(yyruleno==66);
      case 67: /* alu_operator ::= OR_OPER */ yytestcase(yyruleno==67);
      case 68: /* alu_operator ::= EXOR_OPER */ yytestcase(yyruleno==68);
      case 69: /* alu_operator ::= SHL_OPER */ yytestcase(yyruleno==69);
      case 70: /* alu_operator ::= SHR_OPER */ yytestcase(yyruleno==70);
      case 94: /* hint ::= COLON TRUE */ yytestcase(yyruleno==94);
      case 95: /* hint ::= COLON FALSE */ yytestcase(yyruleno==95);
      case 97: /* conditional_oper ::= EQUIV */ yytestcase(yyruleno==97);
      case 98: /* conditional_oper ::= NOT_EQUIV */ yytestcase(yyruleno==98);
      case 99: /* conditional_oper ::= LESS */ yytestcase(yyruleno==99);
      case 100: /* conditional_oper ::= LESS_OR_EQUIV */ yytestcase(yyruleno==100);
      case 101: /* conditional_oper ::= GREATER */ yytestcase(yyruleno==101);
      case 102: /* conditional_oper ::= GREATER_OR_EQUIV */ yytestcase(yyruleno==102);
      case 124: /* optional_sign ::= add_or_subtract */ yytestcase(yyruleno==124);
      case 126: /* add_or_subtract ::= ADD_OPER */ yytestcase(yyruleno==126);
      case 127: /* add_or_subtract ::= SUB_OPER */ yytestcase(yyruleno==127);
#line 339 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
}
#line 1406 "mash.c"
        break;
      case 28: /* optional_predicate ::= */
      case 96: /* hint ::= */ yytestcase(yyruleno==96);
      case 125: /* optional_sign ::= */ yytestcase(yyruleno==125);
#line 342 "mash.y"
{
	yygotominor.yy0 = NULL;
}
#line 1415 "mash.c"
        break;
      case 29: /* rest_of_the_code_line ::= complete_code_line */
      case 30: /* rest_of_the_code_line ::= alu_code_line */ yytestcase(yyruleno==30);
      case 31: /* rest_of_the_code_line ::= data_code_line */ yytestcase(yyruleno==31);
      case 32: /* rest_of_the_code_line ::= control_code_line */ yytestcase(yyruleno==32);
      case 33: /* rest_of_the_code_line ::= int_code_line */ yytestcase(yyruleno==33);
      case 34: /* rest_of_the_code_line ::= int_return_code_line */ yytestcase(yyruleno==34);
      case 35: /* rest_of_the_code_line ::= neg_code_line */ yytestcase(yyruleno==35);
      case 36: /* rest_of_the_code_line ::= inv_code_line */ yytestcase(yyruleno==36);
      case 37: /* rest_of_the_code_line ::= inc_code_line */ yytestcase(yyruleno==37);
      case 38: /* rest_of_the_code_line ::= dec_code_line */ yytestcase(yyruleno==38);
      case 39: /* rest_of_the_code_line ::= movr_code_line */ yytestcase(yyruleno==39);
      case 40: /* rest_of_the_code_line ::= movi_code_line */ yytestcase(yyruleno==40);
      case 41: /* rest_of_the_code_line ::= bra_code_line */ yytestcase(yyruleno==41);
      case 42: /* rest_of_the_code_line ::= nop_code_line */ yytestcase(yyruleno==42);
      case 43: /* rest_of_the_code_line ::= flush_code_line */ yytestcase(yyruleno==43);
#line 347 "mash.y"
{
	yygotominor.yy61 = yymsp[0].minor.yy61;
}
#line 1436 "mash.c"
        break;
      case 44: /* complete_code_line ::= alu_instruction REGISTER comma REGISTER comma REGISTER comma immediate */
      case 45: /* complete_code_line ::= data_load_instruction REGISTER comma REGISTER comma REGISTER comma immediate */ yytestcase(yyruleno==45);
      case 46: /* complete_code_line ::= data_stor_instruction REGISTER comma REGISTER comma REGISTER comma immediate */ yytestcase(yyruleno==46);
      case 47: /* complete_code_line ::= control_instruction REGISTER comma REGISTER comma REGISTER comma immediate */ yytestcase(yyruleno==47);
#line 394 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, yymsp[-7].minor.yy0->data.u, yymsp[-6].minor.yy0->data.u, yymsp[-4].minor.yy0->data.u, yymsp[-2].minor.yy0->data.u, yymsp[0].minor.yy79.u, yymsp[-7].minor.yy0->line_num);
}
#line 1446 "mash.c"
        break;
      case 50: /* alu_code_line ::= alu_instruction REGISTER EQUALS REGISTER alu_operator paren_reg_immed_expr */
#line 411 "mash.y"
{
	bool good_match = false;
	switch(yymsp[-5].minor.yy0->type) {
	case T_ADD:  if(yymsp[-1].minor.yy0->type == T_ADD_OPER)  good_match = true; break;
	case T_SUB:  if(yymsp[-1].minor.yy0->type == T_SUB_OPER)  good_match = true; break;
	case T_MULT: if(yymsp[-1].minor.yy0->type == T_MULT_OPER) good_match = true; break;
	case T_DIV:  if(yymsp[-1].minor.yy0->type == T_DIV_OPER)  good_match = true; break;
	case T_MOD:  if(yymsp[-1].minor.yy0->type == T_MOD_OPER)  good_match = true; break;
	case T_AND:  if(yymsp[-1].minor.yy0->type == T_AND_OPER)  good_match = true; break;
	case T_OR:   if(yymsp[-1].minor.yy0->type == T_OR_OPER)   good_match = true; break;
	case T_EXOR: if(yymsp[-1].minor.yy0->type == T_EXOR_OPER) good_match = true; break;
	case T_SHL:  if(yymsp[-1].minor.yy0->type == T_SHL_OPER)  good_match = true; break;
	case T_SHR:  if(yymsp[-1].minor.yy0->type == T_SHR_OPER)  good_match = true; break;
	}
	if(good_match) {
		set_instruction(&yygotominor.yy61, 0, 0, 0, yymsp[-5].minor.yy0->data.u, yymsp[-4].minor.yy0->data.u, yymsp[-2].minor.yy0->data.u, yymsp[0].minor.yy61.low.rC, yymsp[0].minor.yy61.high.u, yymsp[-5].minor.yy0->line_num);
	}
	else {
		STAGE_ERROR(yymsp[-4].minor.yy0->line_num, "The operator, \"%s\", doesn't match the instruction", yymsp[-1].minor.yy0->token_str);
	}
}
#line 1471 "mash.c"
        break;
      case 51: /* alu_instruction ::= ADD */
#line 433 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_ADD;
}
#line 1479 "mash.c"
        break;
      case 52: /* alu_instruction ::= SUB */
#line 437 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_SUB;
}
#line 1487 "mash.c"
        break;
      case 53: /* alu_instruction ::= MULT */
#line 441 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_MULT;
}
#line 1495 "mash.c"
        break;
      case 54: /* alu_instruction ::= DIV */
#line 445 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_DIV;
}
#line 1503 "mash.c"
        break;
      case 55: /* alu_instruction ::= MOD */
#line 449 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_MOD;
}
#line 1511 "mash.c"
        break;
      case 56: /* alu_instruction ::= AND */
#line 453 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_AND;
}
#line 1519 "mash.c"
        break;
      case 57: /* alu_instruction ::= OR */
#line 457 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_OR;
}
#line 1527 "mash.c"
        break;
      case 58: /* alu_instruction ::= EXOR */
#line 461 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_EXOR;
}
#line 1535 "mash.c"
        break;
      case 59: /* alu_instruction ::= SHL */
#line 465 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_SHL;
}
#line 1543 "mash.c"
        break;
      case 60: /* alu_instruction ::= SHR */
#line 469 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_SHR;
}
#line 1551 "mash.c"
        break;
      case 71: /* paren_reg_immed_expr ::= LEFT_PAREN reg_immediate_expr RIGHT_PAREN */
      case 74: /* optional_paren_reg_immed_expr ::= LEFT_PAREN reg_immediate_expr RIGHT_PAREN */ yytestcase(yyruleno==74);
      case 81: /* bracket_reg_immed_expr ::= LEFT_BRACKET reg_immediate_expr RIGHT_BRACKET */ yytestcase(yyruleno==81);
#line 506 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, 0, 0, 0, yymsp[-1].minor.yy61.low.rC, yymsp[-1].minor.yy61.high.u, -1);
}
#line 1560 "mash.c"
        break;
      case 72: /* paren_reg_immed_expr ::= REGISTER */
#line 509 "mash.y"
{
		set_instruction(&yygotominor.yy61, 0, 0, 0, 0, 0, 0, yymsp[0].minor.yy0->data.u, 0, -1);
}
#line 1567 "mash.c"
        break;
      case 73: /* paren_reg_immed_expr ::= immediate */
#line 512 "mash.y"
{
		set_instruction(&yygotominor.yy61, 0, 0, 0, 0, 0, 0, 0, yymsp[0].minor.yy79.u, -1);
}
#line 1574 "mash.c"
        break;
      case 75: /* optional_paren_reg_immed_expr ::= reg_immediate_expr */
#line 520 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, 0, 0, 0, yymsp[0].minor.yy61.low.rC, yymsp[0].minor.yy61.high.u, -1);
}
#line 1581 "mash.c"
        break;
      case 76: /* reg_immediate_expr ::= REGISTER add_or_subtract immediate */
#line 525 "mash.y"
{
	m_word i;
	i.s = (yymsp[-1].minor.yy0->type == T_ADD_OPER) ? yymsp[0].minor.yy79.s : -yymsp[0].minor.yy79.s;
	set_instruction(&yygotominor.yy61, 0, 0, 0, 0, 0, 0, yymsp[-2].minor.yy0->data.u, i.u, -1);
}
#line 1590 "mash.c"
        break;
      case 77: /* reg_immediate_expr ::= REGISTER */
#line 530 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, 0, 0, 0, yymsp[0].minor.yy0->data.u, 0, -1);
}
#line 1597 "mash.c"
        break;
      case 78: /* reg_immediate_expr ::= immediate */
#line 533 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, 0, 0, 0, 0, yymsp[0].minor.yy79.u, -1);
}
#line 1604 "mash.c"
        break;
      case 79: /* data_code_line ::= data_load_instruction REGISTER EQUALS bracket_reg_immed_expr */
#line 538 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, yymsp[-3].minor.yy0->data.u, yymsp[-2].minor.yy0->data.u, 0, yymsp[0].minor.yy61.low.rC, yymsp[0].minor.yy61.high.u, yymsp[-3].minor.yy0->line_num);
}
#line 1611 "mash.c"
        break;
      case 80: /* data_code_line ::= data_stor_instruction bracket_reg_immed_expr EQUALS REGISTER */
#line 541 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, yymsp[-3].minor.yy0->data.u, yymsp[0].minor.yy0->data.u, 0, yymsp[-2].minor.yy61.low.rC, yymsp[-2].minor.yy61.high.u, yymsp[-3].minor.yy0->line_num);
}
#line 1618 "mash.c"
        break;
      case 82: /* data_load_instruction ::= LOAD */
#line 549 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_LOAD;
}
#line 1626 "mash.c"
        break;
      case 83: /* data_load_instruction ::= RLOAD */
#line 553 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_RLOAD;
}
#line 1634 "mash.c"
        break;
      case 84: /* data_stor_instruction ::= STOR */
#line 558 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_STORE;
}
#line 1642 "mash.c"
        break;
      case 85: /* data_stor_instruction ::= RSTOR */
#line 562 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_RSTORE;
}
#line 1650 "mash.c"
        break;
      case 86: /* control_code_line ::= control_instruction control_address_expr COMMA REGISTER conditional_oper REGISTER */
#line 568 "mash.y"
{
	bool good_match = false;
	switch(yymsp[-5].minor.yy0->type) {
	case T_BRAE:  if(yymsp[-1].minor.yy0->type == T_EQUIV)            good_match = true; break;
	case T_BRANE: if(yymsp[-1].minor.yy0->type == T_NOT_EQUIV)        good_match = true; break;
	case T_BRAL:  if(yymsp[-1].minor.yy0->type == T_LESS)             good_match = true; break;
	case T_BRALE: if(yymsp[-1].minor.yy0->type == T_LESS_OR_EQUIV)    good_match = true; break;
	case T_BRAG:  if(yymsp[-1].minor.yy0->type == T_GREATER)          good_match = true; break;
	case T_BRAGE: if(yymsp[-1].minor.yy0->type == T_GREATER_OR_EQUIV) good_match = true; break;
	}
	if(good_match) {
		set_instruction(&yygotominor.yy61, 0, yymsp[-4].minor.yy61.low.hint, 0, yymsp[-5].minor.yy0->data.u, yymsp[-2].minor.yy0->data.u, yymsp[0].minor.yy0->data.u, yymsp[-4].minor.yy61.low.rC, yymsp[-4].minor.yy61.high.u, yymsp[-5].minor.yy0->line_num);
	}
	else {
		STAGE_ERROR(yymsp[-2].minor.yy0->line_num, "The operator, \"%s\", doesn't match the instruction", yymsp[-1].minor.yy0->token_str);
	}
}
#line 1671 "mash.c"
        break;
      case 87: /* control_instruction ::= BRAE */
#line 586 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_BRAE;
}
#line 1679 "mash.c"
        break;
      case 88: /* control_instruction ::= BRANE */
#line 590 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_BRANE;
}
#line 1687 "mash.c"
        break;
      case 89: /* control_instruction ::= BRAL */
#line 594 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_BRAL;
}
#line 1695 "mash.c"
        break;
      case 90: /* control_instruction ::= BRALE */
#line 598 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_BRALE;
}
#line 1703 "mash.c"
        break;
      case 91: /* control_instruction ::= BRAG */
#line 602 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_BRAG;
}
#line 1711 "mash.c"
        break;
      case 92: /* control_instruction ::= BRAGE */
#line 606 "mash.y"
{
	yygotominor.yy0 = yymsp[0].minor.yy0;
	yymsp[0].minor.yy0->data.u = M_BRAGE;
}
#line 1719 "mash.c"
        break;
      case 93: /* control_address_expr ::= LEFT_BRACKET reg_immediate_expr hint RIGHT_BRACKET */
#line 612 "mash.y"
{
	m_uword x = (yymsp[-1].minor.yy0 && yymsp[-1].minor.yy0->data.u) ? 1 : 0;
	set_instruction(&yygotominor.yy61, 0, x, 0, 0, 0, 0, yymsp[-2].minor.yy61.low.rC, yymsp[-2].minor.yy61.high.u, -1);
}
#line 1727 "mash.c"
        break;
      case 103: /* int_code_line ::= INT optional_paren_reg_immed_expr */
#line 647 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, M_INT, 0, 0, yymsp[0].minor.yy61.low.rC, yymsp[0].minor.yy61.high.u, yymsp[-1].minor.yy0->line_num);
}
#line 1734 "mash.c"
        break;
      case 104: /* int_return_code_line ::= IRET */
#line 651 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, M_IRET, 0, 0, 0, 0, yymsp[0].minor.yy0->line_num);
}
#line 1741 "mash.c"
        break;
      case 105: /* neg_code_line ::= NEG REGISTER */
#line 656 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, M_SUB, yymsp[0].minor.yy0->data.u, 0, yymsp[0].minor.yy0->data.u, 0, yymsp[-1].minor.yy0->line_num);
}
#line 1748 "mash.c"
        break;
      case 106: /* inv_code_line ::= INV REGISTER */
#line 660 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, M_EXOR, yymsp[0].minor.yy0->data.u, yymsp[0].minor.yy0->data.u, 0, ~0U, yymsp[-1].minor.yy0->line_num);
}
#line 1755 "mash.c"
        break;
      case 107: /* inc_code_line ::= INC REGISTER */
#line 664 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, M_ADD, yymsp[0].minor.yy0->data.u, yymsp[0].minor.yy0->data.u, 0, 1, yymsp[-1].minor.yy0->line_num);
}
#line 1762 "mash.c"
        break;
      case 108: /* dec_code_line ::= DEC REGISTER */
#line 668 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, M_SUB, yymsp[0].minor.yy0->data.u, yymsp[0].minor.yy0->data.u, 0, 1, yymsp[-1].minor.yy0->line_num);
}
#line 1769 "mash.c"
        break;
      case 109: /* movr_code_line ::= MOVR REGISTER EQUALS REGISTER */
#line 672 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, M_ADD, yymsp[-2].minor.yy0->data.u, yymsp[0].minor.yy0->data.u, 0, 0, yymsp[-3].minor.yy0->line_num);
}
#line 1776 "mash.c"
        break;
      case 110: /* movi_code_line ::= MOVI REGISTER EQUALS immediate */
#line 676 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, M_ADD, yymsp[-2].minor.yy0->data.u, 0, 0, yymsp[0].minor.yy79.u, yymsp[-3].minor.yy0->line_num);
}
#line 1783 "mash.c"
        break;
      case 111: /* bra_code_line ::= BRA control_address_expr */
#line 680 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 1, 0, M_BRAE, 0, 0, yymsp[0].minor.yy61.low.rC, yymsp[0].minor.yy61.high.u, yymsp[-1].minor.yy0->line_num);
}
#line 1790 "mash.c"
        break;
      case 112: /* nop_code_line ::= NOP */
#line 684 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 0, 0, M_ADD, 0, 0, 0, 0, yymsp[0].minor.yy0->line_num);
}
#line 1797 "mash.c"
        break;
      case 113: /* flush_code_line ::= FLUSH */
#line 688 "mash.y"
{
	set_instruction(&yygotominor.yy61, 0, 1, 0, M_BRANE, 0, 0, M_REGISTER_PC, 0, yymsp[0].minor.yy0->line_num);
	current_branch_is_flush = true;
}
#line 1805 "mash.c"
        break;
      case 114: /* immediate ::= number */
      case 116: /* immediate ::= label_immediate */ yytestcase(yyruleno==116);
      case 117: /* immediate ::= identifier_immediate */ yytestcase(yyruleno==117);
      case 120: /* constant_immediate ::= number */ yytestcase(yyruleno==120);
      case 122: /* address_immediate ::= number */ yytestcase(yyruleno==122);
#line 694 "mash.y"
{
	yygotominor.yy79 = yymsp[0].minor.yy79;
}
#line 1816 "mash.c"
        break;
      case 115: /* immediate ::= CHARACTER */
#line 697 "mash.y"
{
	yygotominor.yy79.u = (m_uword)yymsp[0].minor.yy0->token_str[1];
}
#line 1823 "mash.c"
        break;
      case 118: /* label_immediate ::= LABEL */
#line 708 "mash.y"
{
	if(pass == 2) {
		symbol *sym = symbol_table_search(labels, yymsp[0].minor.yy0->token_str);
		if(sym) {
			yygotominor.yy79 = sym->value;
		}
		else {
			STAGE_ERROR(yymsp[0].minor.yy0->line_num, "The label, %s, is undefined", yymsp[0].minor.yy0->token_str);
		}
	}
}
#line 1838 "mash.c"
        break;
      case 119: /* identifier_immediate ::= IDENTIFIER */
#line 721 "mash.y"
{
	if(pass == 2) {
		symbol *sym;
		if((sym = symbol_table_search(constants, yymsp[0].minor.yy0->token_str))) {
			yygotominor.yy79 = sym->value;
		}
		else if((sym = symbol_table_search(variables, yymsp[0].minor.yy0->token_str))) {
			yygotominor.yy79 = sym->value;
		} else {
			STAGE_ERROR(yymsp[0].minor.yy0->line_num, "Unknown identifier \"%s\"", yymsp[0].minor.yy0->token_str);
		}
	}
}
#line 1855 "mash.c"
        break;
      case 121: /* constant_immediate ::= CHARACTER */
#line 739 "mash.y"
{
	yygotominor.yy79.u = (m_uword)yymsp[0].minor.yy0->token_str[1];
	most_recent_const_immed_line_num = yymsp[0].minor.yy0->line_num;
}
#line 1863 "mash.c"
        break;
      case 123: /* number ::= optional_sign NUMBER */
#line 750 "mash.y"
{
	if(yymsp[-1].minor.yy0 && yymsp[-1].minor.yy0->type == T_SUB_OPER) {
		yygotominor.yy79.s = -yymsp[0].minor.yy0->data.s;
	}
	else {
		yygotominor.yy79 = yymsp[0].minor.yy0->data;
	}
	most_recent_const_immed_line_num = yymsp[0].minor.yy0->line_num;
}
#line 1876 "mash.c"
        break;
      default:
      /* (0) mash_program ::= eols lines */ yytestcase(yyruleno==0);
      /* (1) mash_program ::= lines */ yytestcase(yyruleno==1);
      /* (2) eols ::= eols EOL */ yytestcase(yyruleno==2);
      /* (3) eols ::= EOL */ yytestcase(yyruleno==3);
      /* (4) eols ::= error EOL */ yytestcase(yyruleno==4);
      /* (5) lines ::= lines mode_line eols */ yytestcase(yyruleno==5);
      /* (6) lines ::= lines constant_line eols */ yytestcase(yyruleno==6);
      /* (7) lines ::= lines variable_line eols */ yytestcase(yyruleno==7);
      /* (12) lines ::= */ yytestcase(yyruleno==12);
      /* (21) raw_line ::= raw_line raw_value */ yytestcase(yyruleno==21);
      /* (22) raw_line ::= raw_value */ yytestcase(yyruleno==22);
      /* (48) comma ::= COMMA */ yytestcase(yyruleno==48);
      /* (49) comma ::= */ yytestcase(yyruleno==49);
        break;
  };
  yygoto = yyRuleInfo[yyruleno].lhs;
  yysize = yyRuleInfo[yyruleno].nrhs;
  yypParser->yyidx -= yysize;
  yyact = yy_find_reduce_action(yymsp[-yysize].stateno,(YYCODETYPE)yygoto);
  if( yyact < YYNSTATE ){
#ifdef NDEBUG
    /* If we are not debugging and the reduce action popped at least
    ** one element off the stack, then we can push the new element back
    ** onto the stack here, and skip the stack overflow test in yy_shift().
    ** That gives a significant speed improvement. */
    if( yysize ){
      yypParser->yyidx++;
      yymsp -= yysize-1;
      yymsp->stateno = (YYACTIONTYPE)yyact;
      yymsp->major = (YYCODETYPE)yygoto;
      yymsp->minor = yygotominor;
    }else
#endif
    {
      yy_shift(yypParser,yyact,yygoto,&yygotominor);
    }
  }else{
    assert( yyact == YYNSTATE + YYNRULE + 1 );
    yy_accept(yypParser);
  }
}

/*
** The following code executes when the parse fails
*/
#ifndef YYNOERRORRECOVERY
static void yy_parse_failed(
  yyParser *yypParser           /* The parser */
){
  ParseARG_FETCH;
#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sFail!\n",yyTracePrompt);
  }
#endif
  while( yypParser->yyidx>=0 ) yy_pop_parser_stack(yypParser);
  /* Here code is inserted which will be executed whenever the
  ** parser fails */
  ParseARG_STORE; /* Suppress warning about unused %extra_argument variable */
}
#endif /* YYNOERRORRECOVERY */

/*
** The following code executes when a syntax error first occurs.
*/
static void yy_syntax_error(
  yyParser *yypParser,           /* The parser */
  int yymajor,                   /* The major type of the error token */
  YYMINORTYPE yyminor            /* The minor type of the error token */
){
  ParseARG_FETCH;
#define TOKEN (yyminor.yy0)
#line 56 "mash.y"

	Token *t = yyminor.yy0;
	switch(t->type) {
	case T_EOL:
		STAGE_ERROR(t->line_num, "Unexpected end of line");
		break;
	default:
		STAGE_ERROR(t->line_num, "Unexpected %s token, \"%s\"", yyTokenName[t->type], t->token_str);
		break;
	}
#line 1962 "mash.c"
  ParseARG_STORE; /* Suppress warning about unused %extra_argument variable */
}

/*
** The following is executed when the parser accepts
*/
static void yy_accept(
  yyParser *yypParser           /* The parser */
){
  ParseARG_FETCH;
#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sAccept!\n",yyTracePrompt);
  }
#endif
  while( yypParser->yyidx>=0 ) yy_pop_parser_stack(yypParser);
  /* Here code is inserted which will be executed whenever the
  ** parser accepts */
  ParseARG_STORE; /* Suppress warning about unused %extra_argument variable */
}

/* The main parser program.
** The first argument is a pointer to a structure obtained from
** "ParseAlloc" which describes the current state of the parser.
** The second argument is the major token number.  The third is
** the minor token.  The fourth optional argument is whatever the
** user wants (and specified in the grammar) and is available for
** use by the action routines.
**
** Inputs:
** <ul>
** <li> A pointer to the parser (an opaque structure.)
** <li> The major token number.
** <li> The minor token number.
** <li> An option argument of a grammar-specified type.
** </ul>
**
** Outputs:
** None.
*/
void Parse(
  void *yyp,                   /* The parser */
  int yymajor,                 /* The major token code number */
  ParseTOKENTYPE yyminor       /* The value for the token */
  ParseARG_PDECL               /* Optional %extra_argument parameter */
){
  YYMINORTYPE yyminorunion;
  int yyact;            /* The parser action. */
  int yyendofinput;     /* True if we are at the end of input */
#ifdef YYERRORSYMBOL
  int yyerrorhit = 0;   /* True if yymajor has invoked an error */
#endif
  yyParser *yypParser;  /* The parser */

  /* (re)initialize the parser, if necessary */
  yypParser = (yyParser*)yyp;
  if( yypParser->yyidx<0 ){
#if YYSTACKDEPTH<=0
    if( yypParser->yystksz <=0 ){
      /*memset(&yyminorunion, 0, sizeof(yyminorunion));*/
      yyminorunion = yyzerominor;
      yyStackOverflow(yypParser, &yyminorunion);
      return;
    }
#endif
    yypParser->yyidx = 0;
    yypParser->yyerrcnt = -1;
    yypParser->yystack[0].stateno = 0;
    yypParser->yystack[0].major = 0;
  }
  yyminorunion.yy0 = yyminor;
  yyendofinput = (yymajor==0);
  ParseARG_STORE;

#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sInput %s\n",yyTracePrompt,yyTokenName[yymajor]);
  }
#endif

  do{
    yyact = yy_find_shift_action(yypParser,(YYCODETYPE)yymajor);
    if( yyact<YYNSTATE ){
      assert( !yyendofinput );  /* Impossible to shift the $ token */
      yy_shift(yypParser,yyact,yymajor,&yyminorunion);
      yypParser->yyerrcnt--;
      yymajor = YYNOCODE;
    }else if( yyact < YYNSTATE + YYNRULE ){
      yy_reduce(yypParser,yyact-YYNSTATE);
    }else{
      assert( yyact == YY_ERROR_ACTION );
#ifdef YYERRORSYMBOL
      int yymx;
#endif
#ifndef NDEBUG
      if( yyTraceFILE ){
        fprintf(yyTraceFILE,"%sSyntax Error!\n",yyTracePrompt);
      }
#endif
#ifdef YYERRORSYMBOL
      /* A syntax error has occurred.
      ** The response to an error depends upon whether or not the
      ** grammar defines an error token "ERROR".  
      **
      ** This is what we do if the grammar does define ERROR:
      **
      **  * Call the %syntax_error function.
      **
      **  * Begin popping the stack until we enter a state where
      **    it is legal to shift the error symbol, then shift
      **    the error symbol.
      **
      **  * Set the error count to three.
      **
      **  * Begin accepting and shifting new tokens.  No new error
      **    processing will occur until three tokens have been
      **    shifted successfully.
      **
      */
      if( yypParser->yyerrcnt<0 ){
        yy_syntax_error(yypParser,yymajor,yyminorunion);
      }
      yymx = yypParser->yystack[yypParser->yyidx].major;
      if( yymx==YYERRORSYMBOL || yyerrorhit ){
#ifndef NDEBUG
        if( yyTraceFILE ){
          fprintf(yyTraceFILE,"%sDiscard input token %s\n",
             yyTracePrompt,yyTokenName[yymajor]);
        }
#endif
        yy_destructor(yypParser, (YYCODETYPE)yymajor,&yyminorunion);
        yymajor = YYNOCODE;
      }else{
         while(
          yypParser->yyidx >= 0 &&
          yymx != YYERRORSYMBOL &&
          (yyact = yy_find_reduce_action(
                        yypParser->yystack[yypParser->yyidx].stateno,
                        YYERRORSYMBOL)) >= YYNSTATE
        ){
          yy_pop_parser_stack(yypParser);
        }
        if( yypParser->yyidx < 0 || yymajor==0 ){
          yy_destructor(yypParser,(YYCODETYPE)yymajor,&yyminorunion);
          yy_parse_failed(yypParser);
          yymajor = YYNOCODE;
        }else if( yymx!=YYERRORSYMBOL ){
          YYMINORTYPE u2;
          u2.YYERRSYMDT = 0;
          yy_shift(yypParser,yyact,YYERRORSYMBOL,&u2);
        }
      }
      yypParser->yyerrcnt = 3;
      yyerrorhit = 1;
#elif defined(YYNOERRORRECOVERY)
      /* If the YYNOERRORRECOVERY macro is defined, then do not attempt to
      ** do any kind of error recovery.  Instead, simply invoke the syntax
      ** error routine and continue going as if nothing had happened.
      **
      ** Applications can set this macro (for example inside %include) if
      ** they intend to abandon the parse upon the first syntax error seen.
      */
      yy_syntax_error(yypParser,yymajor,yyminorunion);
      yy_destructor(yypParser,(YYCODETYPE)yymajor,&yyminorunion);
      yymajor = YYNOCODE;
      
#else  /* YYERRORSYMBOL is not defined */
      /* This is what we do if the grammar does not define ERROR:
      **
      **  * Report an error message, and throw away the input token.
      **
      **  * If the input token is $, then fail the parse.
      **
      ** As before, subsequent error messages are suppressed until
      ** three input tokens have been successfully shifted.
      */
      if( yypParser->yyerrcnt<=0 ){
        yy_syntax_error(yypParser,yymajor,yyminorunion);
      }
      yypParser->yyerrcnt = 3;
      yy_destructor(yypParser,(YYCODETYPE)yymajor,&yyminorunion);
      if( yyendofinput ){
        yy_parse_failed(yypParser);
      }
      yymajor = YYNOCODE;
#endif
    }
  }while( yymajor!=YYNOCODE && yypParser->yyidx>=0 );
  return;
}
